﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace CoreMVCSandbox.Controllers
{
    public class CustomRouterExampleController : Controller
    {
        public IActionResult Index()
        {
            return Ok("You hit the custom router example!");
        }
    }
}