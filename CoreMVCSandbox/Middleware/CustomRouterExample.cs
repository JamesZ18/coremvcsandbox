﻿using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreMVCSandbox.Middleware
{
    public class CustomRouterExample : IRouter
    {
        private IRouter _defaultRouter;

        public CustomRouterExample(IRouter defaultRouter)
        {
            _defaultRouter = defaultRouter;
        }

        // Used by framework to build urls
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            return _defaultRouter.GetVirtualPath(context);
        }

        // RouteAsync(context) is part of the IRouter interface.  For this example,
        // RouteAsync checks to see if the request path includes CustomRouterExample
        // within it, and then sets the controller and action to ones created 
        // specifically for this example.
        public async Task RouteAsync(RouteContext context)
        {
            var path = context.HttpContext.Request.Path.Value.Split('/');
          
            if (path.Contains<string>("CustomRouterExample"))
            {
                context.RouteData.Values["controller"] = "CustomRouterExample";
                context.RouteData.Values["action"] = "Index";

                await _defaultRouter.RouteAsync(context);
            }
        }
    }
}
