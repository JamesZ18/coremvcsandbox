﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;

namespace CoreMVCSandbox.Middleware
{
    // TODO: Split up logs once a certain size is reached.
        // TODO: Specify maximum number of log files.
        // TODO: Decide when max number is reached.  Archiving or deletion.
    // TODO: Come up with schema which organizes logs by date.
        // This could be done by adding the date to the name or using subfolders.
    // TODO: Print request body in a more readable manner.
        // Maybe read the stream into an array and add a crlf after some number of characters
    // TODO: Implement some asynchronicity!
    public class CustomLoggingMiddleware
    {
        private readonly RequestDelegate next;

        // The constructor accepts a RequestDelegate to be able to pass the request to the next middleware in the
        // pipeline.
        public CustomLoggingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        // Invoke contains the logic for the middleware.
        public Task Invoke(HttpContext context)
        {            
            if (context != null)
            {
                LogRequestDetails(context.Request);
            }
            return next(context);
        }

        /// <summary>
        /// LogRequestDetails(request) appends the time, method, headers, and body of request to Logs\Middleware.txt
        /// </summary>
        /// <param name="request">HttpRequest object to be logged</param>
        private void LogRequestDetails(HttpRequest request)
        {
            string logPath = System.Environment.CurrentDirectory + "\\Logs\\CustomMiddlewareLog.txt";
            Debug.WriteLine($"Writing to {logPath}");
            using (StreamWriter writer = File.AppendText(logPath))
            {
                writer.WriteLine("New request: \r\n");
                writer.WriteLine($"\tTime: {DateTime.Now}");
                writer.WriteLine($"\tMethod: {request.Method}");
                writer.WriteLine("\tHeaders:");
                foreach (var keyValuePair in request.Headers)
                {
                    writer.WriteLine($"\t\t{keyValuePair.Key}: {keyValuePair.Value}");
                }
                writer.WriteLine("\tBody:");
                writer.WriteLine($"\t\t{StreamToString(request.Body)}\r\n");               
            }
        }

        private string StreamToString(Stream stream)
        {
            string streamOutput = null;
            using (StreamReader reader = new StreamReader(stream))
            {
                streamOutput = reader.ReadToEnd();
            }
            return streamOutput;
        }
    }
    // Contains an extension method for IApplicationBuilder to make calling the custom middleware simpler.
    public static class CustomLoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UseCustomLogger(this IApplicationBuilder app)
        {
            return app.UseMiddleware<CustomLoggingMiddleware>();
        }
    }
}
